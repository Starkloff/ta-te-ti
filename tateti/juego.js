// import * as Jugador from "./tateti/jugador/jugador";
// import { Movimiento } from "./tateti/movimiento.js";
//ids de las celdas en el html
var turno = 1;
var queTurno;
var arreglotateti = new Array(9);
var celdas = document.getElementsByClassName("tateti");
var nombreJugador1 = "";
var nombreJugador2 = "";
var ganados1 = 0;
var ganados2 = 0;
var hola;
var empatados = 0;
var hayganador = false;

// Profesor en la proxima version se separara todos lo que incide en el HTML con la logica de juego.
class Juego {
    constructor(
        // nrojuego,
        // jugador = new Jugador,
        // movimiento = new Movimiento
    ) {

    }
}
    
function eventos() {
    for (var c of celdas){
        c.style.display = "inline-block";
        // en la proxima version se va a trasladar esta funcionalidad al HTML usando un onclick
        c.addEventListener("click", tateti);
    }
}

function tateti(evento) {
    var celda = evento.target;
    celda.removeEventListener("click", tateti);
    var idCelda = celda.id;
    var posicionAMarcar = idCelda[1] - 1;
    queTurno = turno % 2;
    if (queTurno != 0) {
        celda.innerHTML = "X";
        celda.style.color = "green";
        arreglotateti[posicionAMarcar] = "X";
        ganador("X", nombreJugador1);
    } else if (queTurno == 0) {
        celda.innerHTML = "O";
        celda.style.color = "red";
        arreglotateti[posicionAMarcar] = "O";
        ganador("O", nombreJugador2);
    } 
    if(turno === 9 && hayganador === false){
        empatados = empatados +1;
        document.getElementById("e1").value = empatados;
        document.getElementById("e2").value = empatados;
        alert("Empataron");
        document.getElementById("e1").innerHTML = "Empates:" + empatados;
        document.getElementById("e2").innerHTML = "Empates:" + empatados;
        revancha();
    }
    turno++

  


}
function declararNombre() {
    nombreJugador1 = document.getElementById("j1").value;
    nombreJugador2 = document.getElementById("j2").value;
    document.getElementById("jugador1").style.display = "inline-block";
    document.getElementById("jugador2").style.display = "inline-block";
    document.getElementById("jugadores").style.display = "none";
    document.getElementById("nj1").innerHTML = nombreJugador1;
    document.getElementById("nj2").innerHTML = nombreJugador2;
    document.getElementById("g1").innerHTML = "Partidas ganadas: " + ganados1;
    document.getElementById("g2").innerHTML = "Partidas ganadas: " + ganados2;
    document.getElementById("e1").innerHTML = "Empates:" + empatados;
    document.getElementById("e2").innerHTML = "Empates:" + empatados;
    document.getElementById("final").style.display = "inline-block";
    eventos();
}

function ganador(letra, player) {
    if (arreglotateti[0] == letra && arreglotateti[1] == letra && arreglotateti[2] == letra ||
        arreglotateti[3] == letra && arreglotateti[4] == letra && arreglotateti[5] == letra ||
        arreglotateti[6] == letra && arreglotateti[7] == letra && arreglotateti[8] == letra ||
        arreglotateti[0] == letra && arreglotateti[3] == letra && arreglotateti[6] == letra ||
        arreglotateti[1] == letra && arreglotateti[4] == letra && arreglotateti[7] == letra ||
        arreglotateti[2] == letra && arreglotateti[5] == letra && arreglotateti[8] == letra ||
        arreglotateti[0] == letra && arreglotateti[4] == letra && arreglotateti[8] == letra ||
        arreglotateti[2] == letra && arreglotateti[4] == letra && arreglotateti[6] == letra) {
        if(letra === "X"){
            ganados1 = ganados1 + 1;
            document.getElementById("g1").textContent = "Partidas ganadas: " + ganados1;
            hayganador = true;
            alert('El jugador: ' + player + " Ganó");
            revancha();

        } else if (letra === "O"){
            ganados2 = ganados2 + 1;
            document.getElementById("g2").textContent = "Partidas ganadas: " + ganados2;
            hayganador = true;
            alert('El jugador: ' + player + " Ganó");
            revancha();
        }
    } 
}
function finalizar(){
    window.location.reload();
}
function revancha(){
    turno = 0;
    hayganador = false;
    for ( var i =0; i<= 9; i++){
        arreglotateti[i] = null;
    }
    for (var c of celdas){
        c.innerHTML = "";
    }
    eventos();
}


